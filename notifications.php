<?php 
// Include the connection file
include 'php/connect.php';
error_reporting(0) ;
session_start();
$uname = $_SESSION['login_user'];
// redirect on session nnot set
if (!isset($_SESSION["login_user"]))
   {
      header("location: login.php");
   }

?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" type="text/css" href="css/main.css">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css.map.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css.map.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-grid.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-grid.css.map.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-grid.min.css">
    <link rel="stylesheet" href="css/bootstrap-grid.min.css.map.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-reboot.ss">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-reboot.css.map.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-reboot.min.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-reboot.min.css.map.css">

    <script src="js/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="js/bootstrap.bundle.js"></script>
    <script src="js/bootstrap.bundle.js.map.js"></script>
    <script src="js/bootstrap.bundle.min.js"></script>
    <script src="js/bootstrap.bundle.min.js.map.js"></script>
    <script src="js/bootstrap.js"></script>
    <script src="js/bootstrap.js.map.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/bootstrap.min.js.map.js"></script>

    <title>Home:::</title>

    <!-- Popup -->
    <script type="text/javascript">
    var val = "<?php echo $reg; ?>";
    if (val==1){
      alert("user Checked in successfully!");
    }
    </script>

  </head>

  <body>
    
<!--====================== NAVBAR MENU START===================-->
    
  
<nav class="navbar navbar-inverse navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
        <h4> EMPLOYEE MANAGEMENT SYSTEM </h4>
    </div>
  </div>
</nav>
<br>


    <div class="container">
      <SPAN class="text-center">
      </SPAN>
    </div>
    <div class="container">

    <div class="card">
              <div class="card-header">
                <ul class="nav nav-tabs card-header-tabs">
                  <li class="nav-item">
                    <a class="nav-link " href="index.php">Employee Check-In >></a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="employee.php">Employees Management</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link " href="salaries.php">Payroll Management</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link active " href="notifications.php">Notifications</a>
                  </li>
                   <li class="nav-item">
                    <a class="nav-link " href="rates.php">Employee rates</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link  " href="checkout.php"><< Checkout</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="php/logout_exec.php">Logout</a>
                  </li>
                  <li class="nav-item">
                    User : <?php echo $uname; ?>
                  </li>
                </ul>
              </div>
              <div class="card-body">

   <div class="card text-white bg-default mb-3" >
      <!-- <div class="card-header">Header</div> -->
<div class="card-body">

  <div class="panel panel-default">
      <div class="panel-body">

           <div class="tab-pane active" id="inbox">
            <div class="container">
                 <div class="content-container clearfix">
                     <div class="col-md-12">
                         <h1 class="content-title">Inbox</h1>
                         
                         <input type="search" placeholder="Search Mail" class="form-control mail-search" />
                         
                         <ul class="mail-list">

                          <?php
                              $sql = "SELECT * FROM notification ORDER BY date DESC";
                              $result = $conn->query($sql);
                              
                              if ($result->num_rows > 0) {
                                  // echo "<table><tr><th>ID</th><th>Name</th></tr>";
                                  // output data of each row
                                  while($row = $result->fetch_assoc()) {
                                  echo '
                                  <li>
                                       <a href="">
                                           <span class="mail-sender">'.$row["sender"].'</span>
                                           <span class="mail-subject">Notificaton!</span>
                                           <span class="mail-message-preview">'.$row["message"].'</span>
                                           <span class="mail-sender">Date: '.$row["date"].'</span>
                                       </a>
                                   </li>';
                                  }
                              } else {
                                  echo "0 results";
                              }
                          ?>  
                         </ul>
                     </div>
                 </div>
             </div>
            
        </div>
        <hr>

          <form accept-charset="UTF-8" action="php/post.php" method="POST">
              <textarea class="form-control counted" name="message" placeholder="Type in your message" rows="5" style="margin-bottom:10px;"></textarea>
                <select name="reciever"  class="form-control ">  
                    <option selected>Public</option>
                    <!-- select all users from database -->
                    <?php
                            $sql = "SELECT * FROM users ORDER BY fname DESC";
                            $result = $conn->query($sql);

                            if ($result->num_rows > 0) {
                                // echo "<table><tr><th>ID</th><th>Name</th></tr>";
                                // output data of each row
                                while($row = $result->fetch_assoc()) {
                                echo '<option> '.$row["username"].' </option>';
                                }
                            } else {
                                echo "0 results";
                            }
                            $conn->close();
                      ?>     
                  </select>
                  <br>

              <button class="btn btn-info" type="submit">Post New Message</button>
          </form>
      </div>
  </div>  

  
</div>
</div>

    
</body>

 <footer class="text-center">  
<a href="login.php">Logout</a><hr>
Copyright &copy Employee Management, Designed by: <strong>Joy</strong> Koech
  </footer>
</html>
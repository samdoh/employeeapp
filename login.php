<?php 
// Include the connection file
include 'php/connect.php';
error_reporting(0);
?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" type="text/css" href="css/main.css">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css.map.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css.map.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-grid.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-grid.css.map.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-grid.min.css">
    <link rel="stylesheet" href="css/bootstrap-grid.min.css.map.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-reboot.ss">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-reboot.css.map.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-reboot.min.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-reboot.min.css.map.css">

    <script src="js/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="js/bootstrap.bundle.js"></script>
    <script src="js/bootstrap.bundle.js.map.js"></script>
    <script src="js/bootstrap.bundle.min.js"></script>
    <script src="js/bootstrap.bundle.min.js.map.js"></script>
    <script src="js/bootstrap.js"></script>
    <script src="js/bootstrap.js.map.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/bootstrap.min.js.map.js"></script>

    <title>Login</title>

  </head>
  <body>

    <!--====================== NAVBAR MENU START===================-->
    
  
<nav class="navbar navbar-inverse navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
        <h4> EMPLOYEE MANAGEMENT SYSTEM </h4>
    </div>
  </div>
</nav>

   <br>
    <div class="container">
      <span class="text-center">
         <h3>EmployeeManager Systems</h3>
         <h5>Welcome</h5>
      </span>
      <div class="row">
        <div class="col-lg-3">
        </div>
        <div class="col-lg-6">
          <div class="card border-info mb-3">
            <div class="card-header">Login Form</div>
            <div class="card-body text-info">
              <h5 class="card-title">Fill this form to login</h5>
              <form class="" action="php/login_exec.php" method="post">
                <div class="form-group">
                  <label for="exampleInputEmail1">Username</label>
                  <input type="text" name="username" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                  <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Password</label>
                  <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                </div>
                <div class="form-check">
                  <input type="checkbox" class="form-check-input" id="exampleCheck1">
                  <label class="form-check-label" for="exampleCheck1">Remember me...</label>
                </div>
                <div class="text-center"><button type="submit" class="btn btn-primary">Login >></button></div >
                <br>
              </form>
            </div>
            <div class="card-footer text-info">
                  <span>Don't have an account, signup </span><a href="signup.php">HERE</a>
                </div>
          </div>
        </div>
        <div class="col-lg-3">
          
        </div>
     </div>

    
  </body>

</html>



<?php
// error_reporting();
   include("php/connect.php");
   
   if($_SERVER["REQUEST_METHOD"] == "POST") {
      // username and password sent from form 
      
      $myusername = mysqli_real_escape_string($conn,$_POST['username']);
      $mypassword = mysqli_real_escape_string($conn,$_POST['password']); 
      
      $sql = "SELECT id FROM users WHERE username = '$myusername' and password = '$mypassword'";
      $result = mysqli_query($conn,$sql);
      $row = mysqli_fetch_array($result,MYSQLI_ASSOC);
      
      $count = mysqli_num_rows($result);
      if($count == 1) {
         header("location: index.php");
      }else {
         $error = "Your Login Name or Password is invalid";
      }
   }
?>
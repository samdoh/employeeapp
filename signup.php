<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <link rel="stylesheet" type="text/css" href="css/main.css">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css.map.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css.map.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-grid.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-grid.css.map.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-grid.min.css">
    <link rel="stylesheet" href="css/bootstrap-grid.min.css.map.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-reboot.ss">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-reboot.css.map.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-reboot.min.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-reboot.min.css.map.css">

    <script src="js/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="js/bootstrap.bundle.js"></script>
    <script src="js/bootstrap.bundle.js.map.js"></script>
    <script src="js/bootstrap.bundle.min.js"></script>
    <script src="js/bootstrap.bundle.min.js.map.js"></script>
    <script src="js/bootstrap.js"></script>
    <script src="js/bootstrap.js.map.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/bootstrap.min.js.map.js"></script>

    <title>Signup</title>

  </head>
  <body>
    <!--====================== NAVBAR MENU START===================-->
    
  
<nav class="navbar navbar-inverse navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
        <h4> EMPLOYEE MANAGEMENT SYSTEM </h4>
    </div>
  </div>
</nav>
<br>
   
    <div class="container">
      <span class="text-center">
         <h3>EmployeeManager Systems</h3>
         <h5>Create User</h5>
      </span>
      <div class="row">
        <div class="col-lg-3">
        </div>
        <div class="col-lg-6">
          <div class="card border-info mb-3">
            <div class="card-header">Signup Form</div>
            <div class="card-body text-info">
              <h5 class="card-title">Fill this form to login</h5>
              <form class="" action="php/add_user.php" method="post">
                <div class="form-group">
                  <label for="exampleInputEmail1">Full Name</label>
                  <input type="text" name="fname" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="e.g John Joe">
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Nation ID/Passport</label>
                  <input type="text" name="id" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="e.g 210511125">
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">User Name</label>
                  <input type="text" name="uname" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="e.g admin">
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Provided PIN</label>
                  <input type=text" name="pin" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="e.g 2345">
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Email address</label>
                  <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="e.g admin@domain.com">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Password</label>
                  <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Verify Password</label>
                  <input type="password" name="cpassword" class="form-control" id="exampleInputPassword1" placeholder="Password">
                </div>
                <div class="text-center">
                  <button type="button" class="btn btn-default"><a href="login.php">Cancel</a></button>
                  <span>|</span>
                  <button type="submit" class="btn btn-primary">Register >></button>
                </div >
              </form>
            </div>
          </div>
        </div>
        <div class="col-lg-3">
          
        </div>
     </div>

    
  </body>



</html>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css.map.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css.map.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-grid.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-grid.css.map.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-grid.min.css">
    <link rel="stylesheet" href="css/bootstrap-grid.min.css.map.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-reboot.ss">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-reboot.css.map.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-reboot.min.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-reboot.min.css.map.css">

    <script src="js/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="js/bootstrap.bundle.js"></script>
    <script src="js/bootstrap.bundle.js.map.js"></script>
    <script src="js/bootstrap.bundle.min.js"></script>
    <script src="js/bootstrap.bundle.min.js.map.js"></script>
    <script src="js/bootstrap.js"></script>
    <script src="js/bootstrap.js.map.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/bootstrap.min.js.map.js"></script>

    <title>Home:::</title>

    <!-- Popup -->
    <script type="text/javascript">
    var val = "<?php echo $reg; ?>";
    if (val==1){
      alert("user Checked in successfully!");
    }
    </script>

  </head>

  <body>
    <div class="container">
      <SPAN class="text-center">
        <h4> EMPLOYEE MANAGEMENT SYSTEM </h4>
      </SPAN>
    </div>
    <div class="container">
            <div class="card">
              <div class="card-header">
                <ul class="nav nav-tabs card-header-tabs">
                  <li class="nav-item">
                    <a class="nav-link active " href="index.php">Employee Check-In >></a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link " href="employee.php">EMPLOYEES</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link disabled" href="salaries.php">SALARIES</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link disabled" href="notifications.php">NOTIFICATIONS</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link disabled" href="profile.php">PROFILE</a>
                  </li>
                   <li class="nav-item">
                    <a class="nav-link disabled" href="rates.php">NEW RATE</a>
                  </li>
                </ul>
              </div>
              <div class="card-body">
                <h5 class="card-title">Employee Register on Entry</h5>
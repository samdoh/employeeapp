<?php 
// Include the connection file
include 'php/connect.php';
error_reporting(0) ;
session_start();
$uname = $_SESSION['login_user'];
// redirect on session nnot set
if (!isset($_SESSION["login_user"]))
   {
      header("location: login.php");
   }


// for success message
$rate = $_GET['rate'];
?>

<?php include 'php/connect.php'; ?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" type="text/css" href="css/main.css">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css.map.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css.map.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-grid.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-grid.css.map.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-grid.min.css">
    <link rel="stylesheet" href="css/bootstrap-grid.min.css.map.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-reboot.ss">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-reboot.css.map.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-reboot.min.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-reboot.min.css.map.css">

    <script src="js/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="js/bootstrap.bundle.js"></script>
    <script src="js/bootstrap.bundle.js.map.js"></script>
    <script src="js/bootstrap.bundle.min.js"></script>
    <script src="js/bootstrap.bundle.min.js.map.js"></script>
    <script src="js/bootstrap.js"></script>
    <script src="js/bootstrap.js.map.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/bootstrap.min.js.map.js"></script>

    <title>Home:::</title>

    <!-- Popup -->
    <script type="text/javascript">
    var val = "<?php echo $rate; ?>";
    if (val==1){
      alert("Rate added successfully!");
    }
    </script>

  </head>
  <body>

    <!--====================== NAVBAR MENU START===================-->
    
  
<nav class="navbar navbar-inverse navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
        <h4> EMPLOYEE MANAGEMENT SYSTEM </h4>
    </div>
  </div>
</nav>
<br>
    <div class="container">
    </div>
    <div class="container">
      
          <div class="card">
              <div class="card-header">
                <ul class="nav nav-tabs card-header-tabs">
                  <li class="nav-item">
                    <a class="nav-link  " href="index.php">Employee Check-In >></a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link " href="employee.php">Employees Management</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link disabled" href="salaries.php">Payroll Management</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link disabled" href="notifications.php">Notifications</a>
                  </li>
                   <li class="nav-item">
                    <a class="nav-link active" href="rates.php">Employee rates</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link disabled " href="checkout.php"><< Checkout</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="php/logout_exec.php">Logout</a>
                  </li>
                  <li class="nav-item">
                    User : <?php echo $uname; ?>
                  </li>
                </ul>
              </div>
            <div class="row">
              <div class="col-lg-5">
              <div class="card-body">
                <h5 class="card-title">Add New Employee Rate</h5>
                    <div class="card text-white bg-primary mb-3" >
                     <!-- <div class="card-header">Header</div> -->
                     <div class="card-body">
                <form class="" action="php/add_rate.php" method="post">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Enter New Rate::</label>
                    <select name="employee_name"  class="form-control">  
                        <option selected>Select:::</option>
          
                  <!-- select all users from database -->
                  <?php
                          $sql = "SELECT * FROM users ORDER BY fname DESC";
                          $result = $conn->query($sql);

                          if ($result->num_rows > 0) {
                              // echo "<table><tr><th>ID</th><th>Name</th></tr>";
                              // output data of each row
                              while($row = $result->fetch_assoc()) {
                              echo '<option> '.$row["id"].' </option>';
                              }
                          } else {
                              echo "0 results";
                          }
                          $conn->close();
                    ?>     
                    </select>
                    
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Rate Per Day in Ksh.</label>
                    <input type="text" name="rate" class="form-control"/>
                  </div><hr>
                  <div class="text-center"><button type="submit" class="btn btn-default">Add Rate >></button></div >
                  <br>
                </form>
                     </div>
                  </div>
              </div>
            </div>
            <div class="col-lg-7">
              <table class="table">
                <thead>
                  <tr>
                    <th scope="col">ID Number</th>
                    <th scope="col">Rate Per Day</th>
                    <th scope="col">Date</th>
                  </tr>
                </thead>
                <tbody>

                <?php
                  $servername = "localhost";
                  $username = "root";
                  $password = "";
                  $dbname = "employee";

                  // Create connection
                  $conn = new mysqli($servername, $username, $password,$dbname);

                  // Check connection
                  if ($conn->connect_error) {
                      die("Connection failed: " . $conn->connect_error);
                  } 
                  // echo "Connected successfully";

                        $query = "SELECT * FROM salaries ORDER BY date DESC";
                        $result_one = $conn->query($query);

                        if ($result_one->num_rows > 0) {
                            // echo "<table><tr><th>ID</th><th>Name</th></tr>";
                            // output data of each row
                            while($row_one = $result_one->fetch_assoc()) {
                            echo
                            '<tr>
                            <th scope="row"> '.$row_one["emp_id"].'</th>
                            <td>'.$row_one["rate_per_day"].'</td>
                            <td>'.$row_one["date"].'</td>
                            </tr>';
                            }
                        } else {
                            echo "0 results";
                        }
                        $conn->close();
                  ?>     
                      
                </tbody>
              </table>
            </div>
        </div>
        
      </div>            
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    
  </body>


  <footer class="text-center">  
<a href="login.php">Logout</a><hr>
Copyright &copy Employee Management, Designed by: <strong>Joy</strong> Koech
  </footer>



</html>
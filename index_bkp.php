<?php 
// Include the connection file
include 'php/connect.php';
// for success message
?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" type="text/css" href="css/main.css">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css.map.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css.map.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-grid.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-grid.css.map.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-grid.min.css">
    <link rel="stylesheet" href="css/bootstrap-grid.min.css.map.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-reboot.ss">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-reboot.css.map.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-reboot.min.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-reboot.min.css.map.css">

    <script src="js/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="js/bootstrap.bundle.js"></script>
    <script src="js/bootstrap.bundle.js.map.js"></script>
    <script src="js/bootstrap.bundle.min.js"></script>
    <script src="js/bootstrap.bundle.min.js.map.js"></script>
    <script src="js/bootstrap.js"></script>
    <script src="js/bootstrap.js.map.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/bootstrap.min.js.map.js"></script>

    <title>Home:::</title>

    <!-- Popup -->
    <script type="text/javascript">
    var val = "<?php echo $reg; ?>";
    if (val==1){
      alert("user Checked in successfully!");
    }
    </script>

  </head>

  <body>
    <div class="container">
      <SPAN class="text-center">
        <h4> EMPLOYEE MANAGEMENT SYSTEM </h4>
      </SPAN>
    </div>
    <div class="container">
            <div class="card">
              <div class="card-header">
                <ul class="nav nav-tabs card-header-tabs">
                  <li class="nav-item">
                    <a class="nav-link active " href="index.php">Employee Check-In >></a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link " href="employee.php">Employees Management</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link disabled" href="salaries.php">Payroll Management</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link disabled" href="notifications.php">Notifications</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link disabled" href="profile.php">User Management</a>
                  </li>
                   <li class="nav-item">
                    <a class="nav-link disabled" href="rates.php">Employee rates</a>
                  </li>
                </ul>
              </div>
              <div class="card-body">
                <h5 class="card-title">Employee Register on Entry</h5>



                    <div class="card text-white bg-default mb-3" >
                     <!-- <div class="card-header">Header</div> -->
                     <div class="card-body">
                     <h5 class="card-title">Select employee first</h5>
                <form class="" action="php/checkin_user.php" method="post">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Select Employee here::</label>
                    <select name="employee_name"  class="form-control">  
                        <option selected>Select:::</option>
          
<!-- select all users from database -->
          <?php

          $sql = "SELECT * FROM users ORDER BY fname DESC";
          $result = $conn->query($sql);

          if ($result->num_rows > 0) {
              // echo "<table><tr><th>ID</th><th>Name</th></tr>";
              // output data of each row
              while($row = $result->fetch_assoc()) {
              echo '<option> '.$row["fname"].' </option>';
              }
          } else {
              echo "0 results";
          }
          $conn->close();
    ?>
    </select>
            </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Comment</label>
                    <textarea name="comments" class="form-control">  

                    </textarea>
                  </div><hr>
                  <div class="text-center"><button type="submit" class="btn btn-default">Register  Session >></button></div >
                  <br>
                </form>
                    </div>
                  </div>

                  <table class="table">
  <thead>
    <tr>
      <th scope="col">Employee Name</th>
      <th scope="col">Comment</th>
      <th scope="col">Date & Time</th>
    </tr>
  </thead>
  <tbody>
<?php

          $sql = "SELECT * FROM users ORDER BY id ASC";
          $result = $conn->query($sql);

          if ($result->num_rows > 0) {
              // echo "<table><tr><th>ID</th><th>Name</th></tr>";
              // output data of each row
              while($row = $result->fetch_assoc()) {
              echo
              '<tr>
              <th scope="row"> '.$row["id"].'</th>
              <td>'.$row["fname"].'</td>
              <td>'.$row["username"].'</td>
              <td>'.$row["pin"].'</td>
              <td>'.$row["password"].'</td>
              <td>'.$row["email"].'</td>
              <td>'.$row["created_date"].'</td>
              </tr>';
              }
          } else {
              echo "0 results";
          }
          $conn->close();
    ?> 
        
  </tbody>
</table>
              </div>
            </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    
  </body>

<footer class="text-center">  
<a href="login.php">Logout</a><hr>
Copyright &copy Employee Management, Designed by: <strong>Joy</strong> Koech
  </footer>


</html>
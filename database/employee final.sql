-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 25, 2018 at 10:43 PM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `employee`
--

-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

CREATE TABLE `notification` (
  `id` int(11) NOT NULL,
  `sender` varchar(50) NOT NULL,
  `reciever` varchar(50) NOT NULL,
  `message` text NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `payroll`
--

CREATE TABLE `payroll` (
  `id` int(11) NOT NULL,
  `employee` varchar(50) NOT NULL,
  `amount` decimal(10,0) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `register`
--

CREATE TABLE `register` (
  `id` int(11) NOT NULL,
  `emp_name` text NOT NULL,
  `comments` text NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `checkin` int(11) NOT NULL DEFAULT '1',
  `checkout` int(11) NOT NULL DEFAULT '0',
  `timeout` varchar(50) NOT NULL,
  `amount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `register`
--

INSERT INTO `register` (`id`, `emp_name`, `comments`, `date`, `checkin`, `checkout`, `timeout`, `amount`) VALUES
(1, 'Cossy Name', ' \r\n                      ', '2018-07-25 02:21:32', 1, 0, '', 0),
(2, 'Irene Jerop', ' \r\n                      ', '2018-07-25 02:21:37', 1, 1, '2018-07-25 16:33:35', 0),
(3, 'Marry Akinyi', ' \r\n                      ', '2018-07-25 02:21:42', 1, 1, '2018-07-25 21:09:12', 0),
(4, 'Joy Koech', ' \r\n                      ', '2018-07-25 02:21:51', 1, 1, '1532510536', 0),
(5, 'Irene Jerop', ' \r\n                      asasas', '2018-07-25 02:51:00', 1, 1, '2018-07-25 16:33:35', 0),
(9, 'Cossy Name', ' asasasa', '2018-07-25 09:40:36', 1, 0, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `salaries`
--

CREATE TABLE `salaries` (
  `emp_id` int(11) NOT NULL,
  `rate_per_day` decimal(10,0) NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `salaries`
--

INSERT INTO `salaries` (`emp_id`, `rate_per_day`, `date`) VALUES
(82323223, '399', '2018-07-25 10:16:13'),
(98756785, '100', '2018-07-25 08:27:51'),
(111111111, '500', '2018-07-25 12:09:40');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `fname` text NOT NULL,
  `username` text NOT NULL,
  `pin` varchar(4) NOT NULL,
  `password` text NOT NULL,
  `email` text NOT NULL,
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `fname`, `username`, `pin`, `password`, `email`, `created_date`) VALUES
(72323232, 'Cossy Name', 'final', '1234', 'pass', 'millrivermusicstore@gmail.com', '2018-07-17 17:35:11'),
(82323223, 'Marry  Akinyi', 'marryakinyi2018', '2343', 'marry123', 'marry@yahoo.com', '2018-07-17 19:10:10'),
(98756785, 'Irene Jerop', 'Irene', '1122', 'password', 'irenejerop@gmail.com', '2018-07-25 02:02:15'),
(111111111, 'Joy Koech', 'admin', '1111', 'admin', 'admin@gmail.com', '2018-07-17 16:54:36');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `notification`
--
ALTER TABLE `notification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payroll`
--
ALTER TABLE `payroll`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `register`
--
ALTER TABLE `register`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `salaries`
--
ALTER TABLE `salaries`
  ADD PRIMARY KEY (`emp_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `notification`
--
ALTER TABLE `notification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `payroll`
--
ALTER TABLE `payroll`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `register`
--
ALTER TABLE `register`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=98756786;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
